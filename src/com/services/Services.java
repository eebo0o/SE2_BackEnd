package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.models.DBConnection;
import com.models.UserModel;

@Path("/")
public class Services {

	@POST
	@Path("/signup")
	@Produces(MediaType.TEXT_PLAIN)
	public String signUp(@FormParam("name") String name,
			@FormParam("email") String email, @FormParam("pass") String pass) {
		UserModel user = UserModel.addNewUser(name, email, pass);
		return user.getJSONObject();
	}

	@POST
	@Path("/login")
	@Produces(MediaType.TEXT_PLAIN)
	public String login(@FormParam("email") String email,
			@FormParam("pass") String pass) {
		UserModel user = UserModel.login(email, pass);
		return user.getJSONObject();
	}
	
	@POST
	@Path("/updatePosition")
	@Produces(MediaType.TEXT_PLAIN)
	public String updatePosition(@FormParam("id") String id,
			@FormParam("lat") String lat, @FormParam("long") String lon) {
		Boolean status = UserModel.updateUserPosition(Integer.parseInt(id), Double.parseDouble(lat), Double.parseDouble(lon));
		JSONObject json = new JSONObject();
		json.put("status", status ? 1 : 0);
		return json.toJSONString();
	}
	
	
	@POST
	@Path("/follow")
	@Produces(MediaType.TEXT_PLAIN)
	public String Follow(@FormParam("follower_id") String follower ,  @FormParam ("followed_id") String followed) {
		boolean status = UserModel.follow(Integer.parseInt(follower) , Integer.parseInt(followed));
		JSONObject json = new JSONObject();
		json.put("status", status ? 1 : 0);
		return json.toJSONString();
	}

	@POST
	@Path("/unfollow")
	@Produces(MediaType.TEXT_PLAIN)
	public String Unfollow(@FormParam("follower_id") String follower ,  @FormParam ("followed_id") String followed) {
		boolean status = UserModel.unfollow(Integer.parseInt(follower) , Integer.parseInt(followed));
		JSONObject json = new JSONObject();
		json.put("status", status ? 1 : 0);
		return json.toJSONString();
	}

	@POST
	@Path("/getFollowers")
	@Produces(MediaType.TEXT_PLAIN)
	public String Getfollowers(@FormParam("followed_id") String followed_id ) {
		ArrayList<UserModel> users = UserModel.getFollowers(Integer.parseInt(followed_id) );
		JSONArray jsons = new JSONArray();
		String JSONString = "" ;
		for (int i = 0 ; i < users.size() ; i++)
			{
				JSONString += users.get(i).getJSONObject();
			}
		System.out.println(JSONString);
		return JSONString;
	}

	
	
	@POST
	@Path("/getPosition")
	@Produces(MediaType.TEXT_PLAIN)
	public String login(@FormParam("id") String id) {
		UserModel user = UserModel.getUserPosition(Integer.parseInt(id));
		JSONObject json = new JSONObject();
		json.put("lat", user.getLat());
		json.put("long", user.getLon());
		return json.toJSONString();
	}

	@GET
	@Path("/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getJson() {
		return "Hello after editing";
		// Connection URL:
		// mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/
		
		
	
	}
}
